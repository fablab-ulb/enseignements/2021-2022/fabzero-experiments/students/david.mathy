Welcome on the personal webpage of David Mathy, a lazy bioengineer and fabzero student. Take a deep breath and dive into this incredible journey in numerical manufacturing and prototyping. You'll be able to follow all the stages of my learning and project through quick documentation and reports. Enjoy !


## About me

![Happy guy collecting some research material](images/IMG_7134.jpeg)

My Super Power ? Being as passionated about anything as my lazyness level. In other words, I fall in love with literally every discovery (even non-science related !) but the downside is that you cannot ask me to do or learn something I'm not in love with. 

## My background

a portuguese blood with a brusseleir's soul and 25 years old. I'm finishing this year my master of bioengineer in agricultural science which means that I failed a few years in the process but thats fine because failure is synonym of success right ? I love cows, give table tennis lessons in my club ("TT Zenith Brussels" located in Saint-Gilles, feel free to join us !) and sail on a fantastic catamaran every summer-autumn at a competitive level.

## Previous work
Since my tenagehood I've been trying to turn every single wardrobe of my house into a cheap phytotron to grow fancy plants in a controlled environnement. 

Lately I've been working (and doing my thesis) on wheat's old strain genetics, demystifying how diverse they really are and their use in a new breeding approach called "Composite Cross Population" targetting organic,  low-input and local agriculture.

### What fabzero project am I going to develop ?

Okay so, considering my background we've got 3 options ranked below :

1. Prototyping a small and low-cost version of a vegetable protein moist extruder to make your own meatless meat at home. This kind of machine produces meatlike fibers structure from just vegetable concentrated protein and water thanks to the power of basic fluid mechanics and thermodynamic concepts. Watch [this video](https://www.youtube.com/watch?v=17zGQCrDJqY) to take a look on how ETH Zurich and its spinoff are working on this subject.

2. Creating a lab instrument to measure, create a standardized procedure and be able to discriminate table tennis blade from each other. There several ways to do it but for example we can just drop a ball and measure the sound emitted upon impact. Why doing this ? There tons of blades on the market ranging from very slow to very fast, super flexible (lots of vibrations) to as stiff as a brick and from soft touch to very hard.  What is the issue then ? Each brand describes them with their own system's reference, often being based on pure marketing bullshit : the worst nightmare for the players and the coaches, forced to buy several blades before finding an appropriate one.

3. Bringing my embedded-growing-wardrobe to a research-quality level. Let's say we would be able to replace perfectly a phytotron that actually costs 20 000 € and make it fully reapairable. That would mean more money to pay researchers and other operation costs, right ?

