# Final Project


The ultimate goal of the course was to develop a project as a proof-of-concept that solves an issue we identified among the 17 sustainable development goals (SDG) of the UN.

## Identifying the issue

As a former bioengineer, I naturally chose to identify a problematic linked to agriculture sustainability which is the Goal 2 of UN's SDGs : End hunger, achieve food security and improved nutrition and promote sustainable. Moreover, I felt concerned by the fact that researchers in the field of agroecology and sustainability struggle to find sufficient funds for their research projects. This often translates in lower sized and less robust experiments, or forces the researcher to become a overloaded one-man-army. Additionally, I had myself an experience in mind during an intership in an experimental farm were an other intern was in charge of walking across the farm's grasslands and measuring grass height. Her goal was to assess grass growth and build an consulting expertise to improve its management and optimize dairy cow's fodder production. She had to take 10 measures per paddock (= subdivided grassland) across 10 hectares for a total of 200 measures. I remembered her complaining that is was uselessly labour intensive because the tool was entirely mechanical and she had to note on a sheet of paper every measure, then encode them on Excel. I instantly thought at that time that we could improve this tool to automate the measures and facilitate data collecting. This issue seems to connect with both of my personal concerns, so we will start from there. Can we improve this tool to facilitate and accelerate grassland management ?


## Why measuring grass height ?

Behind this small question hides a great problematic which is soil's degradation caused by the paradox of our world-market-connected industrial agriculture. The thruth is that the cute cow you see on your morning brick of milk no longer eat grass and if she has the chance to spend time on grassland, the area is undersized and serves only as a parking. Our quest for perpetual yield growth led us to feed animals with anything else than the food they co-evolved with for milleniums. Today, the vast majority of cows are fed with maize and soja. Why would it be so bad ? In fact, Hybrid Maize is a so productive crop that farmers began to plant it in monocultures, repeating the same crop over the years which inevitably lead to soil's erosion, loss of soil's structure and life (eg: fertilty's key living organisms such as earthworms). Grasslands on the other hand prevent soil's erosion and stock carbon by increasing soil's organic matter content. In other terms, feeding cows with Maize and Soja contributes to soil's loss of diversity and Amazonia's deforestation while grasslands restore fertility and increases biodiversity. 

Why the hell don't we feed cows systematically with grass then ? Basically, because grasslands require really good management to be nearly as productive. The secret explanation behind this statement is resumed in the following figure. Grass nutritional value depends heavily on grass growth stage and is strictly correlated to its height: a young grass is full of nitrogen and low in carbon while an old and tall grass is the opposite; the optimum C/N ratio and energy content being just before flowering stage and below 30cm height. How do we manage to bring optimal grass to the cow along the grazing season then ? By implementing rotational grazing ! We subdivide the grassland in several paddocks and instore a rotation of paddock every 2-3 days. We measure grass height, follow the grass daily growth and establish a rotation schedule in order to come back to the first paddock at its optimal grass height (see section below *Quick Sketching* for an example). 

![](images/paper.png)

Besides this practical approach, getting grass height enables to estimate the biomass produced, the fodder intake by cows these two combined help to design more efficiently the farm and reach the economic optimum. For now, all the farmers I met that were using rotational grazing were economically healthy, which is far from being common nowadays. They produce less for sure but they consume less external inputs overall and gain a better margin. A recent paper (see above) explored grass measuring tool's theory and found that 24 measures per hectare was optimal and giving 5% error margin for the mean compressed sward height.


## Existing tools and softwares to measure grass growth
![](images/herbometre.jpg)

Of course I do not reinvent the wheel and the first thing I did was to check what has been done in the market to improve this tool. To my knowledge, there are 3 digital grass height meters on the market:

- Jenquip EC-20 (1200€, picture above) + Pature'Pro software (16€) to visualize the datas
- Grasshopper (1160€) + software (157€)
- Herbomètre électronique (934€) developed by the French National Agricultural Research Institute (INRA)

I couldn't find what technology use Jenquip and Grasshopper but the third one is using an ultra-sonic sensor to obtain the measures. Jenquip and Grasshopper however offer a bluetooth connection to the smartphone in order to exploit satellite data, detect automatically which paddock you are measuring and visualize spatially the data. They are all quite expensive and this may explain why the intern just had a mechanical version costing 102€ yet. No open source version of grass measurement tool has been released nor documented despite being a quite simple technology.


## Quick sketching and paper-prototyping

The next step was then to draw the first ideas and quickly try to prototype it. To do so, a session was organized with the other students to expose this first approach to each other. I drawed the problematic and prototyped with cardboard and paper the first version of the tool I had in mind which is visible on the following picture.

![](images/paper_prototype.png)

The following plan describes the elements of the tool and how it works : a sliding plate will move upward as being pushed by the grass and a time of flight sensor will measure the current distance between it and the plate ; the difference between this measure initial state distance will give the grass height ; a button will allow the user to register the measure value ; the electronic box computes and display the average of the taken measures.

![](images/schema.jpg)

## Time of Flight Sensor Specs

![](images/vma337.jpg)

So I chose to use a time of flight sensor over ultra sonic sensor because it is supposed to be more accurate. How it works ? It emits IR light of 940nm wavelength and use the time of back and forth travel from the sensor to the reflected area to compute the distance between them. The precise sensor I used is the VMA337 integrating the VL53OX sensor. The following graph taken from the [datasheet](https://www.velleman.eu/downloads/29/infosheets/vl53l0x_datasheet.pdf) shows the performance of the sensor actual vs measured distance. We see that accuracy decreases with distance and the sensor shouldn't be use over 1.2 meters in the High Accuracy  default mode, so we have to take this into account. We also see that a white surface is more accurate than a grey surface, which means that we'll be able to fine tune the performance also by choosing a proper reflective material to add on the sliding plate.

![](images/graph.png)

Also, by reading more in depth the datasheet, it seems that outdoor light conditions may affect its capabilities and accuracy. We'll see what will happen in practice !


## The arduino leonardo like CM-Beetle 32U4

Thanks to the advices of Fabzero Mentor Axel Cornu, I chose to work with this little development board compatible with arduino. Super tiny, usb C charging port and Lithium-Ion battery pins allows me to build a very compact box. I decided to keep it on a tiny breadboard in order to facilitate the development and make it newby-friendly.

![](images/cmbeetle.jpg)

## Bill of materials

| Qty |  Description    |  Price  |           Link           
|-----|-----------------|---------|--------------------------
| 1   | Aluminium bar   |  3.99 €| Magasin de bricolage (Brico)
| 1   | CM-Beetle 32U4  |  9.90 €| https://www.gotronic.fr/ 
| 1   | Accu LiPo 3,7 Vcc 1000 mAh  |  9.90 € | https://www.gotronic.fr/  
| 1   | Oled Display 128x64 |  12.50 € | https://www.gotronic.fr/  
| 1   | VL53OX ToF sensor VMA337 |  14.90 € | https://www.whadda.com/ 
| 1   | Mini Breadboard  |  0 € | Ask a friend or print breadboard-less box model and solder  
| 1   | Wires 0.5mm section |  0 € | Ask a friend or recycled
| 2   | 4mm diameter Screw + Bolt | 0 € | Ask a friend or recycled
| 1   | SPDP Switch ON-ON |  0.65 € | https://www.gotronic.fr/  
| 1   | 73g of PLA filament (20€/kg) |  1.46 € | Your favorite fablab   
| 1   | Printing rental fee (7h @ 1.5€/h) |  10.50 € |  Your favorite fablab
|     | TOTAL |  63.80 € |  Quite cheap for a prototype actually !        |

## Electronic wiring

Now that we have all the components, here comes the wiring. A picture is worth a thousand words so you will find a schema made with the free app Cirkit Designer. Please note that I could not find a CM Beetle in any circuit designing app so I just put an arduino leonardo here for the example. However, I believe that our **3.7v lithium-ion battery we got is not enough voltage for a genuine arduino leonardo**. So if you have an arduino leonardo instead of a CM Beetle 32u4, make sure to find the proper battery. The oled display and the VL53OX sensor are wired on the same pin SCL and SDA. The battery is wired to a switch charges automatically -if the switch is on- when the board is connected to a computer or a charger through usb-c.

![](images/cirkit_designer.png)
 

## Openscad designs

Here you will find the objects I designed in openscad to be printed with PLA filament on a 3D printer. Source code is available at the end of the webpage. The main idea was to create pieces that can fit a regular 10mm diameter aluminium bar (8mm internal diameter).
 
![](images/objects.png)




## The ToF sensor is capricious

Initially, I intended to place the sensor within the box. Unfortunately, the sensor was giving false measures when inside, and after several attempts and loss of time redesigning (fixation hole, window, etc.) and reprinting the box, I decided to build its own little box. In this way, it will even be possible to place it lower and gain accuracy by diminishing the measured distance (400-600mm has a better accuracy than 600-800mm). 


![](images/tof_holder.png)

## Final build: mounting, wiring and soldering

After all the components are tested and working properly, it's time to mount them in their cute box. First, wiring and soldering the switch to the battery and put them in the right place. A tiny bit of tape helps holding the battery.

![](images/battery.jpg)

Then, putting the "fake bottom" which hides the battery and serves as a smooth surface for the breadboard. Inserting the aluminium bar in the holder, tight the screw, and wire everything.

![](images/electronics.jpg)

Because of the struggle I had with this sensor, unfortunately I still have to print one piece before the final showdown. Final update will be released as soon as everything is working properly. 

Of course, do not forget our ToF sensor and his nice little box :-)

![](images/ToF.jpg)

A bit of testing and calibration by adjusting a constant directly in the IDE.

![](images/test.jpg)

Final showdown... Tadaaaam !

![](images/showdown.jpg)

## Source code

### Software - Arduino IDE

```
/*
    FILE    : grass_meter.ino
    AUTHOR  : David MATHY <david.mathy@hotmail.com>
    DATE    : 2022-06-15
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    ADAPTED FROM "SingleRanging examples of the VL53L0X API"
*/


#include <Wire.h>
#include <VL53L0X.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Array.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

VL53L0X sensor;
const int button = 9;
int click_flag = true;
int state;
int grass_height = 0;
int calibration = 518;
String text;

// set the maximum number of measures per device initialization
const int ELEMENT_COUNT_MAX = 40;
Array<int, ELEMENT_COUNT_MAX> measures;

//#define HIGH_SPEED
#define HIGH_ACCURACY


void setup()
{
  //Serial.begin(9600);
  Wire.begin();

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
  Serial.println("SSD1306 allocation failed");
  for(;;); // Don't proceed, loop forever
}

  sensor.init();
  sensor.setTimeout(500);

  pinMode(button,INPUT_PULLUP);

#if defined HIGH_SPEED
  // reduce timing budget to 20 ms (default is about 33 ms)
  sensor.setMeasurementTimingBudget(20000);
#elif defined HIGH_ACCURACY
  // increase timing budget to 200 ms
  sensor.setMeasurementTimingBudget(200000);
#endif
}

void loop()
{
  grass_height = sensor.readRangeSingleMillimeters();
  state = digitalRead(button);
  if (state == LOW && click_flag == true) {
  measures.push_back(abs(grass_height-calibration));

  if (sensor.timeoutOccurred()) { Serial.print(" TIMEOUT"); }
  Serial.println();
  click_flag = false;
  }

  else if(state == HIGH) {
  click_flag = true;
  }

 
  display.clearDisplay();

  display.setTextSize(float(1));
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.println("Current height: "+ String(abs(grass_height-calibration)) + "mm");
  display.setCursor(0,15);
  display.println("Last measure: "+ String(measures.back()) + "mm");
  display.setCursor(0,30);
    int average=0;
    int count=0;
    for(int i=0;i<ELEMENT_COUNT_MAX;i++)
    {
            average = average + measures[i];
    }

    count = measures.size();
    if (count == -1) {
      count = 0;
    }

    if (count > 0) {
      average /= count;
    }
    
  display.println("Measures count: " + String(count));
  display.setCursor(0,45);
  display.println("Average height: " + String(average) + "mm");
  display.display();
  
 }
```

### Box - Openscad design

```
/*
    FILE    : cap.scad
    AUTHOR  : David MATHY <david.mathy@hotmail.com>
    DATE    : 2022-06-15
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
*/

box_length = 70;
box_width = 40;
box_height = 15;
box_thickness = 1;
big_diameter = 12;
medium_diameter = 10;
cylinder_height = 15;

//creates the under plate
difference(){
cube([box_length,box_width,box_thickness], center=true);
translate([25,14,0]) cube([8,5,2],center=true);
translate([box_length*3/8,-box_width*1/7,0]) cylinder(d=7, h = 10, center = true, $fn=100);
}

//left wall
translate([-box_thickness/2-box_length/2,0,-box_thickness/2+box_height/2]) rotate([0,90,0]) cube([box_height,box_width,box_thickness], center=true);

//right wall
translate([+box_thickness/2+box_length/2,0,-box_thickness/2+box_height/2]) rotate([0,90,0]) cube([box_height,box_width,box_thickness], center=true);

//back wall
translate([0,+box_thickness/2+box_width/2,-box_thickness/2+box_height/2]) rotate([90,90,0]) cube([box_height,box_length+2*box_thickness,box_thickness], center=true);

//front wall
translate([0,-box_thickness/2-box_width/2,-box_thickness/2+box_height/2]) rotate([90,90,0]) cube([box_height,box_length+2*box_thickness,box_thickness], center=true);

//build the cylinder piece holding to the aluminium bar
difference() {
    translate([0,big_diameter/2+box_width/2,-box_thickness/2]) cylinder(h = cylinder_height, d= big_diameter, $fn=100);
    translate([0,big_diameter/2+box_width/2,-box_thickness/2]) cylinder(h = cylinder_height, d= medium_diameter, $fn=100);
    translate([0,big_diameter/2+box_width/2,-box_thickness/2]) rotate([0,0,90]) translate([4,0,cylinder_height/2]) cube([7,1,cylinder_height], center = true);
}

//build the square part of the holder where the screw goes
difference() {
translate([0,big_diameter/2+box_width/2,0]) rotate([0,0,90]) translate([8.5,0,-box_thickness/2+cylinder_height/2]) cube([7,4,cylinder_height], center = true);
translate([0,big_diameter/2+box_width/2,0]) rotate([0,0,90]) translate([8.5,0,-box_thickness/2+cylinder_height/2]) cube([7,1,cylinder_height], center = true);
translate([0,big_diameter/2+box_width/2,0]) rotate([0,0,90]) translate([8.8,0,-box_thickness/2+cylinder_height/2]) rotate([90,0,0]) cylinder(h = 10, d = 4,$fn=100, center = true);
}

//left holder's support
translate([-16,25,7])rotate([90,90,20]) cube([box_height,box_length/2.8*box_thickness,box_thickness/2], center=true);

//right holder's support
translate([+16,25,7])rotate([90,90,-20]) cube([box_height,box_length/2.8*box_thickness,box_thickness/2], center=true);

```

### Box's cap - Openscap design

```
/*
    FILE    : box.scad
    AUTHOR  : David MATHY <david.mathy@hotmail.com>
    DATE    : 2022-06-15
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
*/

box_height = 17;
box_thickness = 1;
box_length = 72; // box length + 2 * box thickness
box_width = 42; // box width + 2 * box thickness 
cap_height = 17;
gap = 2;

//build the bottom and the future display's window
difference() {
cube([box_length,box_width,box_thickness], center=true);
translate([1,2,0]) cube([27,16,box_thickness], center=true);
}

//builds the left external wall
difference(){
translate([-box_thickness/2-box_length/2,0,-box_thickness/2+box_height/2]) rotate([0,90,0]) cube([box_height,box_width,box_thickness], center=true);
translate([-box_thickness/2-box_length/2,box_width/12,-box_thickness/2+box_height/2]) rotate([0,90,0]) cube([box_height,box_width*1/3,box_thickness], center=true);
}

//build the left internal wall
difference(){
translate([-box_length/2+box_thickness/2,0,-box_thickness/2+box_height/2-gap/2]) rotate([0,90,0]) cube([box_height-gap,box_width,box_thickness], center=true);
translate([-box_length/2+box_thickness/2,box_width/12,-box_thickness/2+box_height/2-gap/2]) rotate([0,90,0]) cube([box_height-gap,box_width*1/3,box_thickness], center=true);
}

//build the right external and internal wall
translate([+box_thickness/2+box_length/2,0,-box_thickness/2+box_height/2]) rotate([0,90,0]) cube([box_height,box_width,box_thickness], center=true);
translate([+box_length/2-box_thickness/2,0,-box_thickness/2+box_height/2-gap/2]) rotate([0,90,0]) cube([box_height-gap,box_width,box_thickness], center=true);

// build the front wall
translate([0,+box_thickness/2+box_width/2,-box_thickness/2+box_height/2]) rotate([90,90,0]) cube([box_height,box_length+2*box_thickness,box_thickness], center=true);
translate([0,box_width/2-box_thickness/2,-box_thickness/2+box_height/2-gap/2]) rotate([90,90,0]) cube([box_height-gap,box_length+2*box_thickness,box_thickness], center=true);

//build the external back wall
difference(){
translate([0,-box_thickness/2-box_width/2,-box_thickness/2+box_height/2]) rotate([90,90,0]) cube([box_height,box_length+2*box_thickness,box_thickness], center=true);
translate([0,-box_thickness/2-box_width/2,-box_thickness/2+box_height/2]) rotate([90,90,0]) cube([box_height,0.80*box_length+2*box_thickness,box_thickness], center=true);
}

//build the internal back wall
difference(){
translate([0,-box_width/2+box_thickness/2,-box_thickness/2+box_height/2-gap/2]) rotate([90,90,0]) cube([box_height-gap,box_length+2*box_thickness,box_thickness], center=true);
translate([0,-box_width/2+box_thickness/2,-box_thickness/2+box_height/2-gap/2]) rotate([90,90,0]) cube([box_height-gap,box_length/10,box_thickness], center=true);
}
```

### Handle - Openscad design

```
/*
    FILE    : handle.scad
    AUTHOR  : David MATHY <david.mathy@hotmail.com>
    DATE    : 2022-06-15
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
    
    !! EXTERNAL OBJECT USED : Turn_Signal_Stalk.stl freely downloaded on https://www.thingiverse.com/thing:5141149, credits to the submitter
*/

cylinder_height = 20;
medium_diameter = 10;
small_diameter = 8;
tiny_diameter = 6;
button_hole = 5;
button_width = 6.5;
button_height = 6;
copper_diameter = 0.5;

//import the handle shape and dig by difference() function the other shapes
difference(){

    translate([-25,0,0])
    hull() {
    import("/Users/davidmathy/Desktop/FabZero/final project/Turn_Signal_Stalk.stl");
    }
    
    translate([0,0,71])
rotate([180,0,0])
cylinder(h = 10, d = button_hole,$fn=100);
    
//build and dig the channel to hide the wires
hull(){
translate([0,0,60])
rotate([170,0,0])
cylinder(h = 70, d = 3.6,$fn=100);
    
translate([0,0,61])
rotate([180,0,0])
cylinder(h = 1, d = 4,$fn=100);
}

// dig the external cylinder for the aluminium bar
cylinder(h = cylinder_height, d= medium_diameter, $fn=100);

//dig the button's cubic place
translate([0,0,63]) cube([button_width+0.4,button_width-0.4,button_height], center = true);

}


// dig the internal cylinder for the aluminium bar
cylinder(h = cylinder_height, d= small_diameter, $fn=100);

```


### Sliding plate - Openscad design

```
/*
    FILE    : sliding_plate.scad
    AUTHOR  : David MATHY <david.mathy@hotmail.com>
    DATE    : 2022-06-15
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
    
*/

cylinder_height = 20;
medium_diameter = 10.2;
big_diameter = 14;
plate_side = 180;

// build the plate
difference(){
cube([plate_side,plate_side,0.6],center = true);
translate([0,0,-1]) cylinder(h = cylinder_height, d= medium_diameter, $fn=100);
}

//build the slider's base
difference() {
    translate([0,0,2.5]) cube([40,40,5], center = true);

    cylinder(h = cylinder_height, d= medium_diameter, $fn=100);
}

//build the slider's cylinder
difference() {
    cylinder(h = cylinder_height, d= big_diameter, $fn=100);

    cylinder(h = cylinder_height, d= medium_diameter, $fn=100);
}
```

### End block - Openscad design

```
/*
    FILE    : end_block.scad
    AUTHOR  : David MATHY <david.mathy@hotmail.com>
    DATE    : 2022-06-15
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
    
*/

cylinder_height = 5;
interior_height = 20;
big_diameter = 12;
medium_diameter = 10;
small_diameter = 8;
tiny_diameter = 6;

difference() {
    cylinder(h = cylinder_height, d= big_diameter, $fn=100);
    cylinder(h = cylinder_height, d= medium_diameter, $fn=100);
}

difference() {
    cylinder(h = interior_height, d= small_diameter, $fn=100);
    cylinder(h = interior_height, d= tiny_diameter, $fn=100);
}

cylinder(h = 2, d= big_diameter, $fn=100);
```

### ToF holder - Openscap design

```
/*
    FILE    : tof_holder.scad
    AUTHOR  : David MATHY <david.mathy@hotmail.com>
    DATE    : 2022-06-15
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
    
*/

big_diameter = 12;
medium_diameter = 9.8;
cylinder_height = 10;

//build the holder
difference() {
    cylinder(h = cylinder_height, d= big_diameter, $fn=100);
    cylinder(h = cylinder_height, d= medium_diameter, $fn=100);
    translate([4,0,5]) cube([7,1,10], center = true);
}

//build the square part with the hole for the screw
difference() {
translate([8.5,0,5]) cube([7,4,10], center = true);
translate([8.5,0,5]) cube([7,1,10], center = true);
translate([8.8,0,5]) rotate([90,0,0]) cylinder(h = 10, d = 4,$fn=100, center = true);
}

//join between the holding part and the box
difference(){
translate([-20,0,5]) cube([30,5,10], center=true);

}

//build the box for the VMA337 VL53OX ToF sensor
difference(){
translate([-40.5,0,5]) cube([11.5,14.5,10],center=true);
translate([-40.5,0,6]) cube([10.5,13.5,10],center=true);
translate([-40.5,0,5]) cube([9.5,12.5,10],center=true);
}
```
### Fake bottom - Openscad design

```
/*
    FILE    : fake_bottom.scad
    AUTHOR  : David MATHY <david.mathy@hotmail.com>
    DATE    : 2022-06-15
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
    
*/

box_length = 50;
box_width = 37.8;
box_height = 10.4;
box_thickness = 1;
cap_height = 10.4;


cube([box_length,box_width,box_thickness], center=true);

difference(){
translate([-box_thickness/2-box_length/2,0,-box_thickness/2+box_height/2]) rotate([0,90,0]) cube([box_height,box_width,box_thickness], center=true);
translate([-box_thickness/2-box_length/2,0,-box_thickness/2+box_height*3/4]) rotate([0,90,0]) cube([box_height/2,box_width,box_thickness], center=true);
}

translate([+box_thickness/2+box_length/2,0,-box_thickness/2+box_height/2]) rotate([0,90,0]) cube([box_height,box_width,box_thickness], center=true);

translate([-9,+box_thickness/2+box_width/2,-box_thickness/2+box_height/2]) rotate([90,90,0]) cube([box_height,box_length+18+2*box_thickness,box_thickness], center=true);

translate([-9,-box_thickness/2-box_width/2,-box_thickness/2+box_height/2]) rotate([90,90,0]) cube([box_height,box_length+18+2*box_thickness,box_thickness], center=true);

```

