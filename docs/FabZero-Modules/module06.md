# 6. Electronics 2 - Fabrication

This week was a painful and frustrating challenge. We learned how to create your own arduino board using a better microcontroller. To do so, we had to solder all the essential components on a printed circuit board (pcb) and put the bootloader inside the chip to make it work.

## The printed circuit board

What is a pcb ? Basically, it is a thin copper sheet that has been milled and isolated to create several electric paths that will connect or not your electronic components. We could have done it ourselves but to buy time we used already-made pcb.

![](../images/pcb.jpg)


## The soldering nightmare

Here comes the problem : solder such small electronic components was nearly impossible. The problem was a combination of many factor : tin's "sticking" ability is proportional to temperature; the pcb absorbs a lot more heat in contrary to the components, which make them heating unevenly; of course the components can't reach high temperatures otherwise they will burn; pcb's copper may look soldered without really being soldered (usually from being initially cold).

![](../images/soldering.jpg)

## The bootloader

Once everything is soldered, you can try to connect to the computer and put the bootloader inside. Sounds cool ? Nop, although they were looking fine, my solders were not working and we will never know why. En of the story.

**Take home message : just buy a pre-assembled board with the chip you want** (arduino uno is not the only board on earth).