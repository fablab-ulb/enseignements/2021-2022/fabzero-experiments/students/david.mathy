# 2. Computer-aided design

In this module we learned how to create our own vectorised 3D object. The numerical *.stl* file created this way allows then to print it with a 3D printer.

## OpenSCad

To do so, I installed OpenSCad on their [official website](https://openscad.org/). In short, OpenSCad is a quick and powerful way to design the 3D object you have in mind but it requires to deconstruct your object in several simple geometric shapes that you will add together and apply transformations on them.

See the example below : by creating a cylinder with a radius of 5 and an other one with a smaller radius, then applying the function *difference() {cylinder 1; cylinder 2}*, the smaller cylinder is substracted to the bigger one, generating a nice hole.

```
difference()
{
    cylinder(h = 10, r = 5, $fn=100);
    cylinder(h = 10, r = 3, $fn=100);
}
```

![](../images/cylinder.png)

## My first object : the fixed-beam

Using this concept, I built my first 3D object : a fixed-beam flexlink that allows to construct a compliant flexible mecanisms using lego bricks. More info about flexlinks [here](flex). You can find the full code below. Globally, I constructed everything from cylinders, even the flexible part which is the fusion of two cylinder far from each other and filled between them by using the function *hull() {cylinder1; cylinder2}*



```
thickness = 8;
big_radius = 4;
small_radius = 3;
holes_space = 1; 
flex_length = 50;
flex_thickness = 1;

//creates the first part of the piece which composed with two holes.
difference() 
{
hull() 
	{
	cylinder(h = thickness, r = big_radius, $fn=100);
    translate([holes_space+(2*small_radius),0,0]) cylinder(h = thickness, r = big_radius, $fn=100);
	}
cylinder(h = thickness, r = small_radius,$fn=100);
translate([holes_space+(2*small_radius),0,0]) cylinder(h = thickness, r = small_radius,$fn=100);
}

//creates the second extremity with two holes
translate([flex_length+(3*small_radius)+big_radius,0,0]) difference() 
{   
hull() 
	{
	cylinder(h = thickness, r = big_radius, $fn=100);
    translate([holes_space+(2*small_radius),0,0]) cylinder(h = thickness, r = big_radius, $fn=100);    
	}
cylinder(h = thickness, r = small_radius,$fn=100);
translate([holes_space+(2*small_radius),0,0]) cylinder(h = thickness, r = small_radius,$fn=100);
}

//creates the flex bar
hull() 
{
    translate([holes_space+(3*small_radius)+flex_thickness,0,0]) cylinder(h = thickness, r = flex_thickness, $fn=100);
    translate([flex_length+(3*small_radius),0,0]) cylinder(h = thickness, r = flex_thickness, $fn=100);
}

```
![](../images/fixed-beam.png)

## Tips and tricks
- Code it in a parametric way by creating variables and then using it for the shapes parameters (e.g. thickness = 8 in the code above, and then using it to define the height of the cylinders). When it's done, you be able to wrap your whole object in a function with the sizes you want as parameters.

- Comment the different parts of your piece that you implement because the code gets big and messy quickly. If you don't, the next time you will open your file to upgrade a specific part you will loose precious time to find it.


