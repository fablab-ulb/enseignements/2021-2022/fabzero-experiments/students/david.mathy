# 1. Documentation

When it comes to frugal sciences and opensource projects, the first skill we have to master is documentation. It is benefecial for yourself, for others that want to learn from your project and also precious as an archive if you forget things or need to reuse some parts in an other project.

To do so, we will use [GitLab](https://about.gitlab.com/) which is a powerful tool to document a project and keep track of your steps.

## How to use git ?

Reinventing the wheel and copy-pasting stuff is not my cup of tea so rather than following a poor self-made tutorial, I advice you to directly try [this tutorial](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/FabZero-Modules/module01/) that learned me the basics of documenting with git.

However, here are my personal tips & tricks : 

- It is tempting to work and write in the browser on the gitlab website but this is risky, slower and you can't work offline.
- Instead, download an opensource markdown text editor with real time preview like [MacDown](https://macdown.uranusjr.com/) and work locally on your files and then update them on the server with command line add, commit and push (see tutorial above).

## Recap

- Get a ssh key, clone your folder and files project locally
- Download an opensource markdown editor to work locally on your files
- You should always do the following whenever you want to work on your project : *cd < your-folder's-path >*,  *git pull*, *git add*, *git commit* and finally *git push*

