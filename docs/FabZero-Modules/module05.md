# 5. Electronics - Prototyping

In this module we got access to microcontrollers such as Arduino and several sensors, breadboards and wires. The goal was to learn how to prototype a small device that uses sensors and active components like a button or a small motor

## Distance sensor

I quickly overviewed the components we had and retained the VL53L0X sensor to serve my arduino. This small electronic module is capable of measuring distance using the "time of flight" of a light ray.


## Digital contactless meter

So what can we do with such sensor ? I turned it into a digital meter with a single button that can be pressed to send to the computer the value of the measured distance. To do so, I opened the VL53L0X code example and tuned it to my taste. I had also to understand what was the Wire.h library used in there so I consulted [this webpage](https://www.arduino.cc/en/reference/wire). Then I needed to learn how to use the button, so I checked [this nice tutorial ](https://www.arduino.cc/en/reference/wire)

![](../images/breadboard.jpg)

Don't pay attention to the capacitor, he's not connected to anything; I just forgot to take it away when I tuned the button wiring example !

### The arduino code that makes everything working :

```
#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;
const int button = 2;
int click_flag = true;
int state;

void setup()
{
  Serial.begin(9600);
  Wire.begin();

  sensor.init();
  sensor.setTimeout(500);

  // increase timing budget to 200 ms
  sensor.setMeasurementTimingBudget(200000);
}

void loop()
{
  state = digitalRead(button);
  
// The button has to be pushed (LOW) for the first time since its previous state
  if (state == LOW && click_flag == true) 
  {
  	Serial.print(sensor.readRangeSingleMillimeters());
  
  	if (sensor.timeoutOccurred()) 
  	{ 
  		Serial.print(" TIMEOUT"); 
  	}
  	
  	Serial.println();
  	click_flag = false;
  }
  
//if the button is unpressed again, then reset the flag
  else if(state == HIGH) 
  {
  click_flag = true;
  }
 }
```
### Verifying the measurements
To do so, I used a 300mm ruler and positionned the device (emitting/receiving part) at 0mm looking horizontally along the ruler. Then, with a piece of paper I checked the measured values at different positions of the ruler.

![](../images/montage_arduino.jpg)

At first, the measured values were not linear, giving accurate values around 30cm wrong values as I was getting closer to the device. In reality, I just forgot to take away the yellow protective film ! After that, it was working like a charm.

### A small coding mistake
At first, my button was not working like I wanted to and was sending datas continuously when pressed and my conditions where written as below :

```
// The button has to be pushed (LOW) for the first time since its previous state
  if (state == LOW && click_flag == true) 
  {
  ...
  }
  
//if the button is unpressed again, then reset the flag
  else
  {
  click_flag = true;
  }
 }
```
You see the mistake ? I wrongly thought that using else was sufficient. In contrary, it reactivated the flag even when the button was still pressed and thus the first *if* condition was validated again. That's why I had to turn it like that :

```
//if the button is unpressed again, then reset the flag
  else if(state == HIGH) 
  {
  click_flag = true;
  }
 }