# 3. Computer-controlled cutting

What a powerful tool ! This technology is really game-changing and enables the full potential of individuals creativity. Despite looking scary and dangerous, learning and usage were quite easy.

## The basics

How does it work ? In short:

- you open the lasercutter's software
- feed it with your 2D vector file
- split different cuts with their own laser intensity and speed (1 color on your drawing = 1 cutting's setting).

How did I learn to use it ? As simply as by following  [this fantastic tutorial](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md).



## Testing the material

An important thing when it comes to lasercutter is to make for each material some kind of patron with settings ranging from low power low speed to high power high speed. This will prevent you from starting again and again your project because of wrong type of cutting (cuts when you want engraves for folding, and vice versa).

![](../images/test_cutting.jpg)


## The forbidden project

I really wanted to cut wood to build my very first own homemade table tennis blade (my child's dream). Unfortunately, the order was quite clear : build a kirigami. So what did could I do ?

**A table tennis racket kirigami !**

To do so, I found some inspiration out of the latest [stiga cybershape blade](https://www.stigasports.com/eu/cybershape-carbon-cybershape-carbon) and started to draw the racket with Adobe Illustrator (sorry, I use this costy software because that's the destiny of a graphics designer's child). Because there were no curves, I could draw it easily with just lines !

![](../images/kirigami_racket.png)

The red lines will be the cuttings and the black ones just engraving in order to be able to fold these parts easily. Now that we have this vectorial file to give orders to our laser cutter, let's find out what result we got...

![](../images/racket_cut.png)

Bingo, looks great, right ? However, it was not that easy. In reality, it required several attempts to achive this result : the cardboard used was not exactly as identical as the one used when testing the parameters, so some folds appeared to be cut instead (the cardboard was slightly thinner than the first one). Not a big problem, I just needed a good intuition to slightly adjust the settings by reducing the power and increasing the speed.

## Conclusion, tips and tricks

- Do not forget to turn on the lasercutter's smoke filter. The machine being noisy doesn't this little guy is on !
- Two material looking identical but from different sources or coming from different temporality may bring you unpleasant surprises, be careful !
- Then the to go message is : always have you "settings tester svg file" with you so you can quickly retest a new material
- Kirigami is really tricky, folds doesn't always behave as you concieved it in your mind. Good luck and stay brave
