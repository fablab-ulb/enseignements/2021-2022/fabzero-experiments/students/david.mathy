# 4. 3D printing

3D printing, the stuff that put stars in our eyes when we were small kids. Despite the fact being quite powerful and versatile, it brings also some annoying constraints, the first one being the the material used -PLA- is not food safe. On top of that, environmental release of microplastics is rising concerns all over the worlds and the ["biodegradable" PLA doesn't make an exception](https://www.sciencedirect.com/science/article/abs/pii/S0048969722021076#!).

## The tutorial
If you are still reading me to this point, you know what is coming in this section. Great people made great tutorials, I don't want to try to fake a self-made one nor spend time to write my own when I myself juste learned from them.

Then, how did I learn to use these machines ? By getting a small tour with explanations from a fablab teacher... then reading [this fantastic class material](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md) while practicing myself on the 3D printer.

## Testing the machine

Okay so the first task was to impose a torture test to the printer. What is a torture test ? It consist of printing a piece containing different parts with different constraints like bridge, holes, spikes, etc. to test the capabilities of the printer regarding the settings. the required time was around 3 hours so we printed one together [Nathalie Wéron](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/) and I.

![](../images/torture_test.jpeg)

What could we notice ? All the tests were quite good and impressive except the stringing test and the angles that showed a clear weakness and the limit of printing "bridges" in the void.

## Printing my very first own piece

Do you remember when we designed in openscad a flexlink ? Well, now it is the time to make use of it. Recap of the tutotrial : lauch prusa slicer, import your 3D file, check the settings, check the original parameters of your 3D piece to match the dimensions you need, then build the code, put it on an SD card, put the SD card in the printer and that's it, bingo ! Child's play.

<iframe width="560" height="315" src="https://www.youtube.com/embed/S98Tebti4aI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Sounds exciting, right ? Then I printed a lego brick (download on thingiverse) because I was missing a real one to hold my flexlink. Then the first problems appeared : except the fact that stringing was intense, the printer was struggling with bridging the holes. 

![](../images/failed_brick.jpeg)

Axel came, did some black wizardry on the printer, the second attempt succeeded and was fitting perfectly with the flexlink. Then I finally found some real lego to test a bit further its flexbility... and I inflicted a non-reversible deformation. Oops!

![](../images/flexlinks.jpeg)


## The compliant-mecanism
Another assignment was to build a [compliant-mechanism](https://en.m.wikipedia.org/wiki/Compliant_mechanism) the flexlinks printed. Because mines are weak and almost broken already, I a nice circle-shaped flexlink and lego pieces printed by the fablab. Nothing genius here, I just wanted to build intuitively a trigger so I got inspired from the very old guns that pirates were carrying in the past.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Bp5KLTg6RSU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

